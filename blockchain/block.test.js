const Block = require('./block');
const { DIFFICULTY } = require('../config');

describe('Block', () => {
    let block, lastBlock, data;

    beforeEach(() => {
        data = 'bar';
        lastBlock = Block.genesis();
        block = Block.mineBlock(lastBlock, data);

    });

    it('sets the `data` to match the input', () => {
        expect(block.data).toEqual(data);
    });

    it('sets the `lastHash` to match the hash of the last block ', () => {
        expect(block.lastHash).toEqual(lastBlock.hash);
    });

    it('generates a hash that matches the DIFFICULTY', () => {
        expect(block.hash.substring(0, block.DIFFICULTY)).toEqual('0'.repeat(block.DIFFICULTY));
        console.log(block.toString());
    });
});